import React from 'react';
import Login from './Login/Login';
import { useState } from 'react';
import UsersPanel from './Login/UsersPanel';

const USERS = [
  {
    id: '101',
    username: 'Timeea Pitan',
    age: 22,
  },
];

const App = () => {
  const [users, setUsers] = useState(USERS);
  const[user, setUser] = useState(true);

  const addUserHandler = (loginData) => {
    setUsers((prevData) => {
      return [...prevData, loginData];
    });
  };


  const userStatus = (value) => {
    setUser(value);
  }

  return (
    <div>
      <Login onAddUser={addUserHandler} onUserStatus={userStatus}/>
      <UsersPanel userList={users} />
    </div>
  );
};

export default App;
