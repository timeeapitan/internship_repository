import React from 'react';
import './UsersPanel.css';
import UserList from './UserList';

const UsersPanel = (props) => {

  return (
    <div className='expenses'>
      <li>
        <UserList userList={props.userList} userStatus={props.userStatus}/>
      </li>
    </div>
  );
};

export default UsersPanel;
