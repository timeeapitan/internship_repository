import React from 'react';
import './Login.css';
import LoginForm from './LoginForm';
import RestrictionPanel from './RestrictionPanel';
import {useState} from 'react';
import RestrictionPanelAge from './RestrictionPanelAge';
import Restriction from './RestrictionAge';

const Login = (props) => {

  const[showModal, setShowModal] = useState(false);
  const[message, setMessage] = useState('');

  const loginFormDataHandler = (loginData) => {
    const uniqueLoginData = {
      ...loginData,
      id: Math.random().toString(),
    };

    console.log(uniqueLoginData);
    props.onAddUser(uniqueLoginData);
  };
  
  const introducedDataHandler = (errorMessage) => {
    setMessage(errorMessage);
    if(errorMessage != ''){
      setShowModal(true);
    } else {
      setShowModal(false);
    }
  }

  return (
    <div className='new-expense'>
      <LoginForm onSaveLoginFormData={loginFormDataHandler} onIntroduceData={introducedDataHandler} />
          {showModal && <RestrictionPanel errorMessage = {message}/>}
    </div>
  );
};

export default Login;
