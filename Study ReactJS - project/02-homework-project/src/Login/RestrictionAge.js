import React from 'react'
import './RestrictionAge.css'

const RestrictionAge = (props) => {
    if(!props.show){
        return null;
    }

    return (
        <div className='modal'>
            <div className='modal-content'>
                <div className='modal-header'>
                    <h4 className='modal-title'>Invalid input</h4>
                </div>
                <div className='modal-body'>
                    Please enter a valid age ({'>'} 0).
                </div>
                <div className='modal-footer'>
                    <button onClick={props.onClose} className='button'>Okay</button>
                </div>
            </div>
        </div>
    )
}

export default RestrictionAge
