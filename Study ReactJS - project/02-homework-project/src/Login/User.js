import React from 'react';
import { useState } from 'react';

const User = (props) => {
  const [user, setUser] = useState('');

  // const clickHandler = () => {
  //   setUser('Timeea');
  // };

  const userData = props.username + ' (' + props.age + ' years old)';

  return (
    <div className='expense-item'>
      <div className='expense-item__description'>
      {props.userStatus && <h2>{userData}</h2>}
      {/* <button onClick={clickHandler}></button> */}
      </div>
    </div>
  );
};

export default User;
