import React from 'react';
import { useState } from 'react';
import './LoginForm.css';

const LoginForm = (props) => {
  const [enteredUsername, setEnteredUsername] = useState('');
  const [enteredAge, setEnteredAge] = useState('');


  const usernameChangedHandler = (event) => {
    setEnteredUsername(event.target.value);
  };

  const ageChangedHandler = (event) => {
    setEnteredAge(event.target.value);
  };

  const submitHandler = (event) => {
    event.preventDefault();

    
    setEnteredAge('');
    setEnteredUsername('');

    let errorMessage = '';

    if(enteredUsername===''){
      errorMessage = 'Please enter a valid name.';
    } 

    if(enteredAge==='' || enteredAge < 0){
      errorMessage += ' Please enter a valid age.';
    }


    if(errorMessage === ''){
      const user = {
        username: enteredUsername,
        age: enteredAge,
      };
  
  
      props.onSaveLoginFormData(user);
    } 
    
      props.onIntroduceData(errorMessage);


  };

  return (
    <form onSubmit={submitHandler}>
      <div className='new-expense__controls'>
        <ul>
          <div className='new-expense__control'>
            <label>Username</label>
            <input type='text' onChange={usernameChangedHandler} value={enteredUsername}/>
          </div>
          <div className='new-expense__control'>
            <label>Age (Years)</label>
            <input type='text' onChange={ageChangedHandler} value={enteredAge}/>
          </div>
          <div className='new-expense__actions'>
            <button type='submit'>Add User</button>
          </div>
        </ul>
      </div>
    </form>
  );
};

export default LoginForm;
