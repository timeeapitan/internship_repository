import React from 'react'
import './RestrictionInput.css'

const RestrictionInput = (props) => {
    if(!props.show){
        return null;
    }

    return (
        <div className='modal'>
            <div className='modal-content'>
                <div className='modal-header'>
                    <h4 className='modal-title'>Invalid input</h4>
                </div>
                <div className='modal-body'>
                    Please enter a valid name and age (non-empty values).
                </div>
                <div className='modal-footer'>
                    <button onClick={props.onClose} className='button'>Okay</button>
                </div>
            </div>
        </div>
    )
}

export default Restriction
