import React from 'react'
import './RestrictionPanelAge.css'

const RestrictionPanelAge = () => {
    const invalidAge = <p>Please enter a valid age ({'>'} 0).</p>;

    return (
        <div className='restriction_panel'>{invalidAge}</div>
    )
}

export default RestrictionPanelAge
