import React from 'react';
import User from './User';
import './UserList.css';

const UserList = (props) => {
  return (
    <ul className='expenses-list'>
      {props.userList.map((user) => (
        <User key={user.id} username={user.username} age={user.age} userStatus={props.userStatus}></User>
      ))}
    </ul>
  );
};

export default UserList;
