import Expenses from './Expenses/Expenses';
import NewExpense from './Expenses/NewExpense';
import { useState } from 'react';

const INITIAL_EXPENSES = [
  {
    id: 'e1',
    title: 'Toilet Paper',
    amount: 94.12,
    date: new Date(2020, 7, 14),
  },
  {
    id: 'e2',
    title: 'New TV',
    amount: 799.49,
    date: new Date(2021, 2, 12),
  },
  {
    id: 'e3',
    title: 'Car Insurance',
    amount: 294.67,
    date: new Date(2021, 2, 28),
  },
  {
    id: 'e4',
    title: 'New Desk (Wooden)',
    amount: 450,
    date: new Date(2021, 5, 12),
  },
  {
    id: 'e5',
    title: 'New New Desk (Wooden)',
    amount: 100,
    date: new Date(2021, 5, 12),
  },

  {
    id: 'e6',
    title: 'iPhone 11 pro max',
    amount: 1,
    date: new Date(2022, 1, 29),
  },
];

function App() {
  const [expenses, setExpenses] = useState(INITIAL_EXPENSES);

  const addExpenseHandler = (enhancedExpenseData) => {
    setExpenses((prevExpense) => {
      return [enhancedExpenseData, ...prevExpense];
    });
  };

  // const addSelectYearHandler = (savedYear) => {
  //   console.log(savedYear);
  // }

  return (
    <div>
      <NewExpense onAddExpense={addExpenseHandler} />
      <Expenses array={expenses} />
    </div>
  );
}

export default App;
