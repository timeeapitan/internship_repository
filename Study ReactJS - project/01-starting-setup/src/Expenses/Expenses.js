import ExpenseItem from './ExpenseItem';
import './Expenses.css';
import Card from '../UI/Card';
import ExpensesFilter from './ExpensesFilter';
import { useState } from 'react';
import ExpensesList from './ExpensesList';
import ExpensesChart from './ExpensesChart';

const Expenses = (props) => {
  const [filterYear, setFilterYear] = useState('2020');

  const selectYear = (savedYear) => {
    setFilterYear(savedYear);
    console.log(savedYear);
  };

  const filteredExpenses = props.array.filter((expense) => {
    return expense.date.getFullYear().toString() === filterYear;
  });

  // let expensesContent = <p>No Expenses Found</p>;
  // if (filteredExpenses.length > 0) {
  //     expensesContent = filteredExpenses.map((item) =>
  //         <ExpenseItem
  //             key={item.id}
  //             title={item.title}
  //             amount={item.amount}
  //             date={item.date}>
  //         </ExpenseItem>
  //     )
  // }

  return (
    <div>
      <li>
        <Card className='expenses'>
          <ExpensesFilter initialYear={filterYear} onSelectYear={selectYear} />
          <ExpensesChart expenses={filteredExpenses} />
          <ExpensesList array={filteredExpenses} />
          {/* {expensesContent} */}
          {/* {filteredExpenses.array.map((item) =>
                    <ExpenseItem
                        key={item.id}
                        title={item.title}
                        amount={item.amount}
                        date={item.date}>
                    </ExpenseItem>
                )}

                {filteredExpenses.length > 0 ?
                    filteredExpenses.array.map((item) =>
                        <ExpenseItem
                            key={item.id}
                            title={item.title}
                            amount={item.amount}
                            date={item.date}>
                        </ExpenseItem>
                    )
                    :
                    <p>No Expenses Found</p>
                }

                {filteredExpenses.length === 0 && <p>No Expenses Found</p>} */}
        </Card>
      </li>
    </div>
  );
};

export default Expenses;
