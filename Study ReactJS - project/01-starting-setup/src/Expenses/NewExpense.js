import ExpenseForm from './ExpenseForm';
import './NewExpense.css';
import { useState } from 'react';

const NewExpense = (props) => {
  const [form, setForm] = useState(true);
  const [isEditing, setIsEditing] = useState(true);

  const saveExpenseDataHandler = (expenseData) => {
    const enhancedExpenseData = {
      ...expenseData,
      id: Math.random().toString(),
    };

    props.onAddExpense(enhancedExpenseData);
  };

  const hideForm = (value) => {
    setForm(value);
  };

  const addVerify = (
    <button
      onClick={() => {
        setForm(false);
      }}
    >
      Add New Expense
    </button>
  );

  const formVerify = (
    <ExpenseForm
      setForm={hideForm}
      onSaveExpenseData={saveExpenseDataHandler}
    ></ExpenseForm>
  );

  const stopEditingHandler = () => {
    setIsEditing(false);
  };

  const startEditingHandler = () => {
    setIsEditing(true);
  };

  return (
    <div className='new-expense'>{form === true ? addVerify : formVerify}</div>
  );

  //   return(
  //       <div className="new-expense">
  //           {isEditing && <ExpenseForm> onSaveExpenseData = {saveExpenseDataHandler} onStopEditing={stopEditingHandler}</ExpenseForm> }
  //           {!isEditing && <button type="button" onClick={startEditingHandler}>Add New Expense</button> }
  //       </div>
  //   )
};

export default NewExpense;
