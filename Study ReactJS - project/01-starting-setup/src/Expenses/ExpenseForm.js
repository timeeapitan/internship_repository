import './ExpenseForm.css';
import { useState } from 'react';

const ExpenseForm = (props) => {
  const [enteredTitle, setEnteredTitle] = useState('');
  const [enteredAmount, setEnteredAmount] = useState('');
  const [enteredDate, setEnteredDate] = useState('');
  const [form, setForm] = useState(true);

  // const [enteredInput, setEnteredInput] = useState({
  //     title: '',
  //     amount: '',
  //     date: ''
  // });

  const titleChangedHandler = (event) => {
    setEnteredTitle(event.target.value);
    // setEnteredInput((prevState) => {
    //     return {
    //         ...prevState,
    //         title: event.target.value
    //     }
    // });
  };

  const amountChangedHandler = (event) => {
    setEnteredAmount(event.target.value);
  };

  const dateChangedHandler = (event) => {
    setEnteredDate(event.target.value);
  };

  const submitHandler = (event) => {
    event.preventDefault();

    const expenseData = {
      title: enteredTitle,
      amount: enteredAmount,
      date: new Date(enteredDate),
    };

    setEnteredTitle('');
    setEnteredAmount('');
    setEnteredDate('');

    props.onSaveExpenseData(expenseData);
  };

  const hideForm = () => {
    props.setForm(true);
  };

  return (
    <form onSubmit={submitHandler}>
      <div className='new-expense__controls'>
        <div className='new-expense__control'>
          <label>Title</label>
          <input
            onChange={titleChangedHandler}
            type='text'
            value={enteredTitle}
          />
        </div>
        <div className='new-expense__control'>
          <label>Amount</label>
          <input
            onChange={amountChangedHandler}
            type='number'
            min='0.01'
            step='0.01'
            value={enteredAmount}
          />
        </div>
        <div className='new-expense__control'>
          <label>Date</label>
          <input
            onChange={dateChangedHandler}
            type='date'
            min='2019-01-01'
            max='2022-12-31'
            value={enteredDate}
          />
        </div>
      </div>
      <div className='new-expense__actions'>
        <button onClick={hideForm}>Cancel</button>
        <button type='submit'>Add Expense</button>
      </div>
    </form>
  );
};

export default ExpenseForm;
