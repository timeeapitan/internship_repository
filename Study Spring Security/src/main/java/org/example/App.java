package org.example;

import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        MvcConfig config = new MvcConfig();
        ViewControllerRegistry registry = new ViewControllerRegistry();
        config.addViewControllers(registry);
    }
}
