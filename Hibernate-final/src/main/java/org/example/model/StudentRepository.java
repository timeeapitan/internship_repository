package org.example.model;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;

public class StudentRepository {

    private EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;

    public StudentRepository() {
        entityManagerFactory = Persistence.createEntityManagerFactory("student");
        this.entityManager = entityManagerFactory.createEntityManager();
    }

    public StudentRepository(String persistenceUnit) {
        entityManagerFactory = Persistence.createEntityManagerFactory(persistenceUnit);
        this.entityManager = entityManagerFactory.createEntityManager();
    }

    public Student addStudent(Student s) {
        entityManager.getTransaction().begin();
        entityManager.persist(s);
        entityManager.getTransaction().commit();
        return s;
    }

    public Student findStudent(Long id) {
        return entityManager.find(Student.class, id);
    }

    public List<Student> getAllStudents() {
        String select = "SELECT s FROM Student s";
        Query query = entityManager.createQuery(select);
        entityManager.getTransaction().begin();
        List<Student> students = query.getResultList();
        return students;
    }

    public Student updateStudent(Student s) {
        Student student = entityManager.find(Student.class, s.getId());

        entityManager.getTransaction().begin();

        student.setLastName(s.getLastName());
        student.setFirstName(s.getFirstName());

        entityManager.getTransaction().commit();

        return student;
    }

    public void deleteStudent(Student s) {
        Student student = entityManager.find(Student.class, s.getId());

        entityManager.getTransaction().begin();
        entityManager.remove(student);
        entityManager.getTransaction().commit();
    }

    public void close() {
        this.entityManager.close();
        this.entityManagerFactory.close();
    }

    public EntityManagerFactory getEntityManagerFactory() {
        return entityManagerFactory;
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }
}
