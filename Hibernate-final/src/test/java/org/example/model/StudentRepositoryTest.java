package org.example.model;

import org.junit.*;

import static org.junit.Assert.*;

public class StudentRepositoryTest {

    private static StudentRepository repository;

    @BeforeClass
    public static void beforeClass() throws Exception {
        repository = new StudentRepository("student_test");
    }

    @AfterClass
    public static void afterClass() throws Exception {

    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void addStudent() {
        Student student = new Student();
        student.setFirstName("Alan");
        student.setLastName("Red");
        repository.addStudent(student);

        assertNotNull(student.getId());
        assertTrue(student.getId().equals(1L));
    }

    @Test
    public void findStudent() {
        Student student = new Student();
        student.setFirstName("Alan");
        student.setLastName("Red");

        repository.addStudent(student);

        student = repository.findStudent(student.getId());

        assertNotNull(student);
        assertNotNull(student.getId());
        assertEquals("Red", student.getLastName());
    }

    @Test
    public void getAllStudents() {
        Student student = new Student();
        student.setFirstName("Alan");
        student.setLastName("Red");

        repository.addStudent(student);

        assertEquals(1, repository.getAllStudents().size());
    }

    @Test
    public void updateStudent() {
        Student student = new Student();
        student.setFirstName("Alan");
        student.setLastName("Red");

        repository.addStudent(student);

        student.setLastName("Green");
        student = repository.updateStudent(student);

        assertNotNull(student);
        assertEquals("Green", student.getLastName());
        assertEquals("Alan", student.getFirstName());
    }

    @Test
    public void deleteStudent() {
        Student student = new Student();
        student.setFirstName("Alan");
        student.setLastName("Red");

        repository.addStudent(student);

        repository.deleteStudent(student);
        student = repository.findStudent(student.getId());

        assertNull(student);
    }

    @Test
    public void close() {
        assertTrue(repository.getEntityManager().isOpen());
        repository.close();
        assertFalse(repository.getEntityManager().isOpen());
    }
}