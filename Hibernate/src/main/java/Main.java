import org.hibernate.Session;
import javax.persistence.Query;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        CarRepo carRepo = new CarRepo();
        System.out.println(carRepo.getCars());

        carRepo.addCars();
    }

    private static void create(Session session){
        Car mustang = new Car();
        mustang.setModel("mustang");
        mustang.setPrice("40,000.00");

        Car audi = new Car();
        audi.setModel("audi");
        audi.setModel("20,000.00");

        session.beginTransaction();
        session.save(mustang);
        session.save(audi);

        session.getTransaction().commit();
    }

    private static void read(Session session){
        Query q = session.createQuery("Select _car from Car _car");
        List<Car> cars = q.getResultList();

        System.out.println("Reading car records...");

        for(Car c: cars){
            System.out.println(c.getModel() + c.getPrice());
        }
    }


    private static void delete(Session session){
        Car audi = session.get(Car.class, "audi");

        session.beginTransaction();
        session.delete(audi);
        session.getTransaction().commit();
    }

    private static void update(Session session){
        Car mustang = session.get(Car.class, "mustang");
        mustang.setPrice("35,250.00");
        mustang.setModel("mustang");

        session.beginTransaction();
        session.update(mustang);
        session.getTransaction().commit();
    }
}
