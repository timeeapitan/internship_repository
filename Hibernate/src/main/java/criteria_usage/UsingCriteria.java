package criteria_usage;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import java.util.List;

public class UsingCriteria {
    public static void main(String[] args) {
        EntityManagerFactory em = Persistence.createEntityManagerFactory("default");
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Item> q = cb.createQuery(Item.class);
        Root<Item> c = q.from(Item.class);

        ParameterExpression<Integer> p = cb.parameter(Integer.class);
        q.select(c).where(cb.gt(c.get("population"), p));

    }
}
