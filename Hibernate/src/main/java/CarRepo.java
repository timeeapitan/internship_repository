import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;

public class CarRepo {
    private final EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");

    public List<Car> getCars(){
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        String sql = "SELECT c FROM Car c";

        Query q = entityManager.createQuery(sql);
        entityManager.getTransaction().begin();

        List list = q.getResultList();
        entityManager.close();

        return list;
    }

    public void addCars(){
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        String sql = "INSERT INTO Car c (id, model, price) VALUES ('1', 'Mustang', '35,000.00')";

        Query q = entityManager.createNativeQuery(sql);
        entityManager.getTransaction().begin();

        q.executeUpdate();
    }
}
