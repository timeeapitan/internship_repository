package hibernate_example;

public class Main {
    public static void main(String[] args) {
        Student student = new Student();
        student.setFirstName("Alan");
        student.setLastName("Red");

        StudentService repository = new StudentService();
        repository.addStudent(student);

        System.out.println("Added student " + student);

        student = repository.findStudent(student.getId());

        System.out.println("Found student " + student);

        student.setLastName("Green");

        repository.updateStudent(student);

        System.out.println("Updated student " + student);

        repository.deleteStudent(student);

        System.out.println("Deleted student " + student);

        System.out.println(repository.getAllStudents());

        repository.close();
    }
}
