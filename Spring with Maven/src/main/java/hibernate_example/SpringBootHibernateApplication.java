package hibernate_example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.logging.*;

@SpringBootApplication
public class SpringBootHibernateApplication {
    private static final Logger logger = Logger.getLogger(SpringBootHibernateApplication.class.getName());

    public static void main(String[] args) {
        SpringApplication.run(SpringBootHibernateApplication.class, args);
        System.out.println(logger);
    }
}
