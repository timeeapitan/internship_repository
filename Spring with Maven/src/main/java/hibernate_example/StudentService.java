package hibernate_example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

@Service
public class StudentService {

    private EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;

    @Autowired
    private StudentRepository studentRepository;

    public StudentService() {
        entityManagerFactory = Persistence.createEntityManagerFactory("student");
        this.entityManager = entityManagerFactory.createEntityManager();
    }

    public StudentService(String persistenceUnit) {
        entityManagerFactory = Persistence.createEntityManagerFactory(persistenceUnit);
        this.entityManager = entityManagerFactory.createEntityManager();
    }

    @Transactional
    public Student addStudent(Student s) {
        entityManager.getTransaction().begin();
        entityManager.persist(s);
        entityManager.getTransaction().commit();
        return s;
    }

    @Transactional
    public Student findStudent(Long id) {
        return entityManager.find(Student.class, id);
    }


    @Transactional
    public List<Student> getAllStudents() {
        String select = "SELECT s FROM Student s";
        Query query = entityManager.createQuery(select);
        entityManager.getTransaction().begin();
        List<Student> students = query.getResultList();
        return students;
    }

    @Transactional
    public Student updateStudent(Student s) {
        Student student = entityManager.find(Student.class, s.getId());

        entityManager.getTransaction().begin();

        student.setLastName(s.getLastName());
        student.setFirstName(s.getFirstName());

        entityManager.getTransaction().commit();

        return student;
    }

    public Student deleteStudent(Student s) {
        Student student = entityManager.find(Student.class, s.getId());

        entityManager.getTransaction().begin();
        entityManager.remove(student);
        entityManager.getTransaction().commit();
        return student;
    }

    @Transactional
    public void close() {
        this.entityManager.close();
        this.entityManagerFactory.close();
    }

    public EntityManagerFactory getEntityManagerFactory() {
        return entityManagerFactory;
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }
}
