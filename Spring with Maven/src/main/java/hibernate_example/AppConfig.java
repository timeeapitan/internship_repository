package hibernate_example;

import org.slf4j.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {
    StudentService repo = new StudentService();

    private static final Logger logger = LoggerFactory.getLogger(AppConfig.class);


    @Bean
    CommandLineRunner initDatabase(final StudentRepository repository){
        return args -> {
            logger.info("Preloading " + repository.save(new Student(1L, "Jack", "Milo")));
            logger.info("Preloading " + repository.save(new Student(2L, "Amanda", "Stewart")));
        };
    }
}
