package hibernate_example;


import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/students")
public class StudentController {

    @Resource(name ="studentService")
    private StudentService studentService;

    @GetMapping
    public List<Student> getStudents(){
        return studentService.getAllStudents();
    }

    @GetMapping("/customer/{id}")
    public Student findStudent(@PathVariable Long id){
        return studentService.findStudent(id);
    }

    @PostMapping("/customer")
    public Student addStudent(@RequestBody Student s){
        return studentService.addStudent(s);
    }


    @DeleteMapping("/customer/{id}")
    public void deleteStudent(@RequestBody Student s){
        studentService.deleteStudent(s);
    }
}
