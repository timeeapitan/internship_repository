package mockito;

public interface StockService {
    double getPrice(Stock stock);
}
