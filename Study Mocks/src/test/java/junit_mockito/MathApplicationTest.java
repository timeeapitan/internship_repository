package junit_mockito;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;


@RunWith(MockitoJUnitRunner.class)
public class MathApplicationTest {

    @InjectMocks // create and inject the mock object
    MathApplication mathApplication = new MathApplication();

    @Mock // create mock object that will be injected
    CalculatorService calculatorService;

    @Test
    public void addTest() {
        when(calculatorService.add(4.0, 5.0)).thenReturn(9.0);
        when(calculatorService.subtract(7.0, 4.0)).thenReturn(3.0);
        when(calculatorService.multiply(3.0, 3.0)).thenReturn(9.0);

        // 3 times
        Assert.assertEquals(calculatorService.add(4.0, 5.0), 9.0, 0);
        Assert.assertEquals(calculatorService.add(4.0, 5.0), 9.0, 0);
        Assert.assertEquals(calculatorService.add(4.0, 5.0), 9.0, 0);


        // 1 time
        Assert.assertEquals(calculatorService.subtract(7.0, 4.0), 3.0, 0);

        // verify(calculatorService).add(3.0, 4.0); -> wrong
        verify(calculatorService, times(3)).add(4.0, 5.0);
        verify(calculatorService).subtract(7.0, 4.0);
        verify(calculatorService, never()).multiply(3.0, 3.0);
    }

}