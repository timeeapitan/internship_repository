package mockito;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PortfolioTest {
    Portfolio portfolio;
    StockService stockService;

    public static void main(String[] args) throws Exception {
        PortfolioTest tester = new PortfolioTest();
        tester.setUp();
        System.out.println(tester.testMarketValue()?"pass":"fail");
    }

    @Before
    public void setUp() throws Exception {
        portfolio = new Portfolio();
        stockService = mock(StockService.class);
        portfolio.setStockService(stockService);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getStockService() {
    }

    @Test
    public void setStockService() {
    }

    @Test
    public void getStocks() {
    }

    @Test
    public void setStocks() {
    }

    @Test
    public boolean testMarketValue() {
        List<Stock> stocks = new ArrayList<Stock>();
        Stock googleStock = new Stock("1", "Google", 10);
        Stock microsoftStock = new Stock("2", "Microsoft", 100);

        stocks.add(googleStock);
        stocks.add(microsoftStock);

        portfolio.setStocks(stocks);

        when(stockService.getPrice(googleStock)).thenReturn(50.00);
        when(stockService.getPrice(microsoftStock)).thenReturn(1000.00);

        double marketValue = portfolio.getMarketValue();
        return marketValue == 100500.0;
    }
}