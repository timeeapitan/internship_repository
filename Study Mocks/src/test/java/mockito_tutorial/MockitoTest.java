package mockito_tutorial;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MockitoTest {

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void notUsingMockAnnotation() {
        List<String> mockList = Mockito.mock(ArrayList.class);

        mockList.add("one");
        Assert.assertEquals(0, mockList.size());

        when(mockList.size()).thenReturn(100);
        Assert.assertEquals(100, mockList.size());
    }

    @Mock
    List mockList;

    @Test
    public void usingMockAnnotation() {
        mockList.add("one");
        Assert.assertEquals(0, mockList.size());

        when(mockList.size()).thenReturn(100);
        Assert.assertEquals(100, mockList.size());
    }


    @Test
    public void notUsingSpyAnnotation() {
        List<String> spyList = Mockito.spy(ArrayList.class);

        spyList.add("one");
        spyList.add("two");

        Mockito.verify(spyList).add("one");
        Mockito.verify(spyList).add("two");

        Assert.assertEquals(2, spyList.size());

        Mockito.doReturn(100).when(spyList).size();

        Assert.assertEquals(100, spyList.size());
    }

    @Spy
    List spyList = new ArrayList();

    @Test
    public void usingSpyAnnotation() {
        spyList.add("one");
        spyList.add("two");

        Mockito.verify(spyList).add("one");
        Mockito.verify(spyList).add("two");

        Assert.assertEquals(2, spyList.size());

        Mockito.doReturn(100).when(spyList).size();
    }

    @Test
    public void notUsingCaptorAnnotation() {
        
    }
}
