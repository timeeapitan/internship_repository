package com.project;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.*;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.PropertyModel;

public class Login extends WebPage {
    private User user = new User("User", "1234");
    private TextField tf;
    private PasswordTextField pw;
    private Button button;
    private boolean rememberMe;
    private CheckBox checkBox;

    public Login() {
        tf = new TextField<>("name", new PropertyModel<>(user, "name"));
        pw = new PasswordTextField("password", new PropertyModel<>(user, "password"));

        Form form = new Form<Void>("contactForm") {
            @Override
            protected void onSubmit() {
                SignInSession session = (SignInSession) getSession();

                if (session.signIn((String) tf.getModelObject(), (String) pw.getModelObject())) {
                    continueToOriginalDestination();
                    setResponsePage(Index.class);
                } else {
                    String errorMessage = getString("loginError", null, "Unable to sign you in!");
                    error(errorMessage);
                }
            }
        };

        checkBox = new CheckBox("rememberMe", new PropertyModel<>(this, "rememberMe"));

        button = new Button("submitButton") {
            @Override
            public void onSubmit() {
                super.onSubmit();
            }
        };

        form.add(tf);
        form.add(pw);
        form.add(button);

        add(form);
    }
}
