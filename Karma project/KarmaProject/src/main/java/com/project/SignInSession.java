package com.project;

import com.project.model.User;
import org.apache.wicket.protocol.http.WebSession;
import org.apache.wicket.request.Request;

public class SignInSession extends WebSession {
//    User user;
//
//    public SignInSession(Request request) {
//        super(request);
//    }
//
//    @Override
//    protected boolean authenticate(String username, String password) {
//        return (user.getName().equalsIgnoreCase(username) && user.getPassword().equalsIgnoreCase(password));
//    }
//
//    @Override
//    public Roles getRoles() {
//        if (isSignedIn()) {
//            return new Roles(Roles.ADMIN);
//        }
//
//        return null;
//    }
//
//    public User getUser() {
//        return user;
//    }
//
//    public void setUser(User user) {
//        this.user = user;
//    }

    private User user;

    public SignInSession(Request request) {
        super(request);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean userLoggedIn() {
        return user != null;
    }

    public boolean userNotLoggedIn(){
        return user == null;
    }

    public static SignInSession get(){
        return (SignInSession) WebSession.get();
    }
}
