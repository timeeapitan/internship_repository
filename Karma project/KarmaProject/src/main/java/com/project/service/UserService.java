package com.project.service;

import com.project.model.User;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UserService {
    private static List<User> users = new ArrayList<>();


    public User findByNameAndPassword(String name, String password){
        for(User user: users){
            if(user.getName().equalsIgnoreCase(name) && user.getPassword().equalsIgnoreCase(password)){
                return user;
            }
        }
        return null;
    }

    public static List<User> getUsers() {
        return users;
    }

    public static void addUser(String name, String password){
       // users.add(User.builder().name(name).password(password).build());
        User user = User.builder().name(name).password(password).build();
        users.add(user);
        System.out.println(name + " " + password);
    }
}
