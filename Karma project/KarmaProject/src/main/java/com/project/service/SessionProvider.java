package com.project.service;

import com.project.SignInSession;
import com.project.model.User;
import org.apache.wicket.protocol.http.WebSession;
import org.apache.wicket.request.Request;

import javax.servlet.http.Cookie;

public class SessionProvider {
    public static final int REMEMBER_ME_DURATION_IN_DAYS = 30;
    public static final String REMEMBER_ME_NAME_COOKIE = "loginCookie";
    public static final String REMEMBER_ME_PASSWORD_COOKIE = "passwordCookie";

    private UserService userService;
    private CookieService cookieService;

    public SessionProvider(UserService userService, CookieService cookieService) {
        this.userService = userService;
        this.cookieService = cookieService;
    }

    public WebSession createNewSession(Request request){
        SignInSession session = new SignInSession(request);

        Cookie nameCookie = cookieService.loadCookie(request, REMEMBER_ME_NAME_COOKIE);
        Cookie passwordCookie = cookieService.loadCookie(request, REMEMBER_ME_PASSWORD_COOKIE);

        if(nameCookie != null && passwordCookie != null){
            User user = userService.findByNameAndPassword(nameCookie.getValue(), passwordCookie.getValue());

            if(user != null){
                session.setUser(user);
                session.info("You were automatically logged in.");
            }
        }

        return session;
    }
}
