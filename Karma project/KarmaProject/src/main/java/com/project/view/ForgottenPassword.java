package com.project.view;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;

public class ForgottenPassword extends WebPage {

    public ForgottenPassword() {
        add(new FeedbackPanel("feedback"));

        Form resetPasswordForm = new Form("resetForm");
        resetPasswordForm.add(new TextField<String>("name", new PropertyModel<>(this, "name")));
        resetPasswordForm.add(new TextField<String>("password", new PropertyModel<>(this, "password")));
        resetPasswordForm.add(new TextField<String>("confirmPassword", new PropertyModel<>(this, "confirmPassword")));

        Button resetButton = new Button("resetPassword"){
            @Override
            public void onSubmit() {

            }
        };
    }
}
