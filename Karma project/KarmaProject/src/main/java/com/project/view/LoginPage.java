package com.project.view;

import com.project.SignInSession;
import com.project.WicketApplication;
import com.project.model.User;
import com.project.service.CookieService;
import com.project.service.UserService;
import org.apache.wicket.feedback.FeedbackMessage;
import org.apache.wicket.feedback.IFeedbackMessageFilter;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.*;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;

import java.util.List;

import static com.project.service.SessionProvider.*;

public class LoginPage extends WebPage {
    private List<User> users = UserService.getUsers();

    private String name;
    private String password;
    private boolean rememberMe;

    public LoginPage() {
        add(new FeedbackPanel("feedbackMessage", new ExactErrorLevelFilter(FeedbackMessage.ERROR)));
        add(new FeedbackPanel("successMessage", new ExactErrorLevelFilter(FeedbackMessage.SUCCESS)));

        Form<Void> form = new Form<>("contactForm"){
            @Override
            protected void onSubmit() {
                super.onSubmit();
                success("Successful login!");
            }
        };
        add(form);

        form.add(new TextField<String>("name", new PropertyModel<>(this, "name")).setRequired(true));
        form.add(new PasswordTextField("password", new PropertyModel<>(this, "password")).setRequired(true));
        form.add(new CheckBox("rememberMe", new PropertyModel<>(this, "rememberMe")));

        Button submitButton = new Button("submitButton") {
            @Override
            public void onSubmit() {
                UserService userService = WicketApplication.get().getUserService();
                User user = userService.findByNameAndPassword(name, password);

                if (user == null) {
                    error("Invalid login");
                } else {
                    SignInSession.get().setUser(user);

                    if (rememberMe) {
                        CookieService cookieService = WicketApplication.get().getCookieService();
                        cookieService.saveCookie(getResponse(), REMEMBER_ME_NAME_COOKIE, user.getName(), REMEMBER_ME_DURATION_IN_DAYS);
                        cookieService.saveCookie(getResponse(), REMEMBER_ME_PASSWORD_COOKIE, user.getPassword(), REMEMBER_ME_DURATION_IN_DAYS);
                    }

                    setResponsePage(Index.class);
                }
            }
        };

        form.add(submitButton);

        add(new Link<Void>("createAccount", new PropertyModel<>(this, "createAccount")) {
            @Override
            public void onClick() {
                setResponsePage(CreateAccount.class);
            }
        });


        form.add(new Link<Void> ("forgotPassword", new PropertyModel<>(this, "forgotPassword")){
            @Override
            public void onClick() {
                setResponsePage(ForgottenPassword.class);
            }
        });
    }


    static class ExactErrorLevelFilter implements IFeedbackMessageFilter {
        private int errorLevel;

        public ExactErrorLevelFilter(int errorLevel) {
            this.errorLevel = errorLevel;
        }

        @Override
        public boolean accept(FeedbackMessage message) {
            return message.getLevel() == errorLevel;
        }
    }
}

