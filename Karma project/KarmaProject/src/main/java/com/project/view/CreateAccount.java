package com.project.view;

import com.project.model.User;
import com.project.service.UserService;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.*;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.ComponentPropertyModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;

import java.util.List;

public class CreateAccount extends WebPage {
    private List<User> users = UserService.getUsers();

    private TextField tf;
    private PasswordTextField pw;
    private TextField email;

    public CreateAccount() {
        User user = User.builder().build();

        add(new FeedbackPanel("feedback"));

        Form<Void> form = new Form<>("createAccountForm");
        add(form);

        tf = new TextField<String>("name", new PropertyModel<>(user, "name"));
        tf.setRequired(true);
        form.add(tf);

        pw = new PasswordTextField("password", new PropertyModel<>(user, "password"));
        pw.setRequired(true);
        form.add(pw);

        email = new TextField<String>("email", new Model(""));
        email.setRequired(true);
        form.add(email);

        Button createAccountButton = new Button("createAccountButton"){
            @Override
            public void onSubmit() {
                String name = (String) tf.getModelObject();
                String password = pw.getModelObject();
                System.out.println(name + " " + password);
                UserService.addUser(name, password);
                setResponsePage(LoginPage.class);
            }
        };

        form.add(createAccountButton);

        System.out.println(users);
    }
}
