package org.example.model;

import org.junit.*;

import java.util.List;

import static org.junit.Assert.*;

public class RepositoryTest {

    private static Repository repo;

    @BeforeClass
    public static void beforeClass() throws Exception {
        repo = new Repository("student");
    }

    @AfterClass
    public static void afterClass() throws Exception {
        repo.close();
    }

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void createAndAddStudent() {
        Student student = new Student();
        student.setFirstName("Emily");
        student.setLastName("Johnson");
        repo.createAndAddStudent(student);
        assertNotNull(student);
        assertTrue(student.getId().equals(1L));
    }

    @Test
    public void findStudent() {
        Student student = new Student();
        student.setFirstName("Emily");
        student.setLastName("Johnson");

        repo.createAndAddStudent(student);

        System.out.println(student.getId());

        student = repo.findStudent(student.getId());
        assertNotNull(student);
        assertNotNull(student.getId());
        assertEquals("Johnson", student.getLastName());
    }

    @Test
    public void getAllStudents() {
        List<Student> students = repo.getAllStudents();

        assertNotNull(students);
        assertTrue(students.size() == 0);
    }

    @Test
    public void updateStudent() {
        Student student = new Student();
        student.setFirstName("Emily");
        student.setLastName("Johnson");

        repo.createAndAddStudent(student);
    }

    @Test
    public void deleteStudent() {
    }
}