package org.example.model;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;

public class Repository {

    private EntityManager entityManager;
    private EntityManagerFactory entityManagerFactory;

    public Repository() {
        entityManagerFactory = Persistence.createEntityManagerFactory("student");
        this.entityManager = entityManagerFactory.createEntityManager();
    }

    public Repository(String persistenceUnit) {
        entityManagerFactory = Persistence.createEntityManagerFactory(persistenceUnit);
        this.entityManager = entityManagerFactory.createEntityManager();
    }

    public void createAndAddStudent(Student s) {
        entityManager.getTransaction().begin();

        Student student = new Student();
        student.setFirstName(s.getFirstName());
        student.setLastName(s.getLastName());

        entityManager.persist(student);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public Student findStudent(Long id) {
        entityManager.getTransaction().begin();

        Student student = entityManager.find(Student.class, id);

        if (student != null) {
            return student;
        }

        return null;
    }

    public List<Student> getAllStudents() {
        String select = "SELECT s FROM Student s";
        Query query = entityManager.createQuery(select);
        entityManager.getTransaction().begin();
        List<Student> students = query.getResultList();
        entityManager.close();
        return students;
    }

    public void updateStudent(Student s) {
        entityManager.getTransaction().begin();

        Student student = entityManager.find(Student.class, 1L);

        student.setLastName(s.getLastName());
        student.setFirstName(s.getFirstName());

        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public void deleteStudent(Student s) {
        Student student = entityManager.find(Student.class, s.getId());

        entityManager.getTransaction().begin();
        entityManager.remove(student);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public void close() {
        this.entityManager.close();
        this.entityManagerFactory.close();
    }
}
