package org.example.model;

public class Main {
    public static void main(String[] args) {
        Repository repository = new Repository();

        Student s = new Student();
        s.setLastName("Doe");
        s.setFirstName("John");
        repository.createAndAddStudent(s);

        s.setLastName("Daniels");
        s.setFirstName("Anne");
        repository.createAndAddStudent(s);

        s.setLastName("Dunkin");
        s.setFirstName("Phil");
        repository.createAndAddStudent(s);

        System.out.println(repository.findStudent(1L));
        System.out.println(repository.getAllStudents());

        repository.updateStudent(s);
        System.out.println(repository.getAllStudents());

        repository.deleteStudent(s);
        System.out.println(repository.getAllStudents());
    }
}
