package method_references;

public class ReferenceToAConstructor {
    public static void main(String[] args) {
        Messageable hello = Message::new;
        hello.getMessage("Hello");
    }
}

interface Messageable{
    Message getMessage(String str);
}

class Message{
    Message(String str){
        System.out.print(str);
    }
}

