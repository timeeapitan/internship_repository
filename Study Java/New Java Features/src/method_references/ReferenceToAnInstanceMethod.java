package method_references;


// ContainingObject::instanceMethodName
public class ReferenceToAnInstanceMethod {
    public static void main(String[] args) {
        ReferenceToAnInstanceMethod methodReference = new ReferenceToAnInstanceMethod();
        Sayable sayable = methodReference::saySomething;
        sayable.say();

        Sayable sayable1 = new ReferenceToAnInstanceMethod()::saySomething;
        sayable1.say();

        Thread t = new Thread(new ReferenceToAnInstanceMethod()::saySomething);
        t.start();
    }

    public void saySomething(){
        System.out.println("A non-static method is invoked.");
    }
}
