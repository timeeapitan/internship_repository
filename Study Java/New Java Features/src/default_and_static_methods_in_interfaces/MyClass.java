package default_and_static_methods_in_interfaces;

public class MyClass implements Interface1, Interface2{
    @Override
    public void method1() {

    }

    @Override
    public void log(String str) {
        System.out.println("MyClass logging: " + str);
        Interface1.print("abc");
    }

    @Override
    public void method2() {

    }
}
