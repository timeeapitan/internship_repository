package exceptions;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class TryCatchExample {
    public static void main(String[] args) {

        // arithmetic exception
        try {
            int data = 50 / 0;
        } catch (Exception ex) {
            System.out.println("We cannot divide by zero.");
        }


        // file not found exception

        PrintWriter pw;
        try {
            pw = new PrintWriter("file.txt");
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } finally {
            System.out.println("finally is executed");
        }


        try {
            System.out.println(1 / 0);
        } catch (ArrayIndexOutOfBoundsException | NumberFormatException | ArithmeticException ex) {
            ex.printStackTrace();
        }

    }
}
