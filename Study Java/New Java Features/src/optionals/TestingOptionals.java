package optionals;

import java.util.Optional;

public class TestingOptionals {
    public static void main(String[] args) {
        TestingOptionals optionals = new TestingOptionals();
        Integer value1 = null;
        Integer value2 = 10;

        // allows passed parameter to be null
        Optional<Integer> a = Optional.ofNullable(value1);


        // throws NullPointerException if passed parameter is null
        Optional<Integer> b = Optional.of(value2);
        System.out.println(optionals.sum(a, b));
    }

    public Integer sum(Optional<Integer> a, Optional<Integer> b) {
        System.out.println("First parameter is present " + a.isPresent());
        System.out.println("Second parameter is present " + b.isPresent());

        Integer value1 = a.orElse(0);
        Integer value2 = 0;

        if(b.isPresent()){
            value2 = b.get();
        }

        return value1 + value2;
    }
}
