package functional_interface;

public class Test {
    public static void main(String[] args) {
        MyFunctionalInterface lambda = () -> {
            System.out.println("Executing...");
        };

        FunctionalInterface result = Test::execute;
        result.execute();
    }

    public static void execute(){
        System.out.println("This is executing.");
    }
}


interface FunctionalInterface{
    void execute();
}