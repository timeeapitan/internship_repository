package functional_interface;

import java.io.IOException;
import java.io.PrintWriter;

public interface MyOtherFunctionalInterface {
    void execute();

    default void print(String text) {
        System.out.println(text);
    }

    static void print(String text, PrintWriter writer) throws IOException{
        writer.write(text);
    }

}
