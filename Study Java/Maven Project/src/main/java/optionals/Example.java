package optionals;

import java.util.Optional;

public class Example {
    public static void main(String[] args) {
        User user = new User(667290, "Kevin Hart");
        Optional<User> userOptional = Optional.of(user);
        Optional<User> nullUserOptional = Optional.empty();

        userOptional.ifPresent(System.out::println);

        System.out.println(userOptional.get());
        System.out.println(nullUserOptional.orElse(new User(0, "Unknown User")));

        User newUser = nullUserOptional.orElseGet(() -> new User(1234567, "Cassandra"));

        System.out.println(newUser);
    }
}

class User {
    String name;
    long id;

    User(long id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return id + " : " + name;
    }
}
