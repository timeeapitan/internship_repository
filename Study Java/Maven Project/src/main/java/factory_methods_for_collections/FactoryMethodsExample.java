package factory_methods_for_collections;

import java.util.List;

public class FactoryMethodsExample {
    public static void main(String[] args) {
        List<String> list = List.of("Java", "JavaFX", "Spring", "Hibernate", "JSP");
        for (String s : list) {
            System.out.println(s);
        }

        // .of() method for Set and Map
    }
}
