package method_references;

import java.util.function.BiFunction;

// ContainingClass::staticMethodName
public class ReferenceToAStaticMethod {
    public static void main(String[] args) {
        Sayable sayable = ReferenceToAStaticMethod::saySomething;
        sayable.say();

        Thread t = new Thread(ReferenceToAStaticMethod::ThreadStatus);
        t.start();

        BiFunction<Integer, Integer, Integer> adder = Arithmetic::add;
        System.out.println(adder.apply(10, 20));
    }

    public static void saySomething() {
        System.out.println("Static method was invoked.");
    }

    public static void ThreadStatus() {
        System.out.println("Thread is running...");
    }
}

interface Sayable {
    void say();
}

class Arithmetic {
    public static int add(int a, int b) {
        return a + b;
    }
}
