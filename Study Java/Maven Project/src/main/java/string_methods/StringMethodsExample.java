package string_methods;

import java.util.List;
import java.util.stream.Collectors;

public class StringMethodsExample {

    public static boolean useIsBlank(String s) {
        return s.isBlank();
    }

    public static List<String> useLines(String s) {
        return s.lines().collect(Collectors.toList());
    }

    public static String useStrip(String s) {
        return s.strip();
    }

    public static String useStripLeading(String s) {
        return s.stripLeading();
    }

    public static String useStripTrailing(String s) {
        return s.stripLeading();
    }

    public static String useRepeat(String s, int n) {
        return s.repeat(n);
    }

}
