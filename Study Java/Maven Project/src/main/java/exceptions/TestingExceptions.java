package exceptions;

public class TestingExceptions {
    private String message;

    public TestingExceptions(String message) {
        this.message = message;
    }

    public void printMessage() throws ArithmeticException{
        System.out.println(message);
        int divide = 1/0;
    }

    public int printResult() throws ArithmeticException{
        return 1 / 0;
    }

    public String printHiMessage() throws ArithmeticException{
        message = "Programming using " + message;
        System.out.println(message);
        return message;
    }

}
