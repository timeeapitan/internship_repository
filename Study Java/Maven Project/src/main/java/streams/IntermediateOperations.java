package streams;

import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class IntermediateOperations {
    public static void main(String[] args) {

        // map()
        Stream<String> strStream = Stream.of("Welcome", "To", "Java", "Blog");
        Stream<String> subStream = strStream
                .map(string -> {
                    if(string.equals("Java")){
                        return "Java is a programming language";
                    }
                    return string;
                });

        ArrayList list = (ArrayList) subStream.collect(Collectors.toList());

        System.out.println(list);
    }
}
