package lambda_expressions;

import java.util.ArrayList;
import java.util.function.Consumer;

public class UsingLambdaExpressions {
    public static void main(String[] args) {
        ArrayList<Integer> numbers = new ArrayList<>();
        for (int i = 1; i <= 5; i++) {
            numbers.add(i);
        }

        numbers.forEach((n) -> {
            System.out.print(n + " ");
        });

        System.out.println();
        Consumer<Integer> method = (n) -> {
            System.out.print(n + " ");
        };

        numbers.forEach(method);

        StringFunction exclaim = (s) -> s + "!";
        StringFunction ask = (s) -> s + "?";

        printFormatted("\nHello", exclaim);
        printFormatted("Hello", ask);
    }

    public static void printFormatted(String str, StringFunction format){
        String result = format.run(str);
        System.out.println(result);
    }
}

interface StringFunction{
    String run(String str);
}
