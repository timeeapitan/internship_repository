package using_files;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ReadingFilesExample {
    public static String readFile(String filename) {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            File myFile = new File(filename);
            Scanner myReader = new Scanner(myFile);

            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                stringBuilder.append(data);
            }

            myReader.close();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }

        return stringBuilder.toString();
    }
}
