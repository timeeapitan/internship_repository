package using_files;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ReadingFilesInJava8 {

    private static final String file = "C:\\Users\\timeea.pitan\\Documents\\test_repository\\Study Java\\Maven Project\\src\\main\\resources\\demo.txt";

    public static void main(String[] args) {

        Path path = Paths.get(file);

        try (Stream<String> stream = Files.lines(path)) {
            stream.forEach(System.out::println);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        String content = null;
        try {
            content = Files.lines(Paths.get(file)).collect(Collectors.joining(System.lineSeparator()));
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        System.out.println(content);
    }
}
