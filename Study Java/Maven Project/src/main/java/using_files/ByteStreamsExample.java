package using_files;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class ByteStreamsExample {

    public static boolean readAndWrite(String filename) throws IOException {

        try (FileInputStream in = new FileInputStream(filename); FileOutputStream out = new FileOutputStream("C:\\Users\\timeea.pitan\\Documents\\test_repository\\Study Java\\Maven Project\\src\\main\\resources\\out.txt")) {

            int c;
            while ((c = in.read()) != -1) {
                out.write(c);
            }
        }
        return true;
    }

    public static void main(String[] args) throws IOException {
        readAndWrite("C:\\Users\\timeea.pitan\\Documents\\test_repository\\Study Java\\Maven Project\\src\\main\\resources\\filename.txt");
    }
}
