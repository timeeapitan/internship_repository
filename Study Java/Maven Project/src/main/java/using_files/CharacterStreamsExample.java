package using_files;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class CharacterStreamsExample {


    public static void readAndWriteFiles(String filename) {
        try (FileReader in = new FileReader(filename); FileWriter out = new FileWriter("C:\\Users\\timeea.pitan\\Documents\\test_repository\\Study Java\\Maven Project\\src\\main\\resources\\out.txt")) {
            int c;
            while ((c = in.read()) != -1) {
                out.write(c);
            }
        } catch (IOException ex) {
            System.out.println("The file hasn't been found");
        }
    }
}
