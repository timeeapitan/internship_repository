package java_time;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class JavaCurrentDateIAndTime {
    public static void main(String[] args) {
        Instant instant = Instant.now();
        System.out.println(instant);

        LocalDateTime now = LocalDateTime.now();
        System.out.println(now);

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mm:ss");
        System.out.println(dtf.format(now));

        ZonedDateTime zonedDateTime = ZonedDateTime.now();
        System.out.println(dtf.format(zonedDateTime));

        Clock clock = Clock.systemDefaultZone();
        instant = clock.instant();
        System.out.println(instant);
    }
}
