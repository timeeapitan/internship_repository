import exceptions.TestingExceptions;
import org.junit.Test;
import org.junit.jupiter.api.function.Executable;
import string_methods.StringMethodsExample;
import using_files.ByteStreamsExample;
import using_files.ReadingFilesExample;

import java.io.IOException;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class Testing {
    static final String filename = "C:\\Users\\timeea.pitan\\Documents\\test_repository\\Study Java\\Maven Project\\src\\main\\resources\\filename.txt";

    @Test
    public void testIsBlank() {
        assertTrue(StringMethodsExample.useIsBlank(""));
    }

    @Test
    public void testLines() {
        assertEquals(StringMethodsExample.useLines("I am using Java"), Arrays.asList("I am using Java"));
    }

    @Test
    public void testStrip() {
        assertEquals(StringMethodsExample.useStrip("   Hello,    \tWorld\t  "), "Hello,    \tWorld");
    }


    @Test
    public void testStripLeading() {
        assertEquals(StringMethodsExample.useStripLeading("   Hello,    \tWorld\t  "), "Hello,    \tWorld\t  ");
    }

    @Test
    public void testStripTrailing() {
        assertEquals(StringMethodsExample.useStripTrailing("   Hello,    \tWorld\t  "), "Hello,    \tWorld\t  ");
    }

    @Test
    public void useRepeat() {
        assertEquals(StringMethodsExample.useRepeat("DoReMi", 3), "DoReMiDoReMiDoReMi");
    }

    @Test
    public void testFileReading() {
        assertEquals(ReadingFilesExample.readFile(filename), "Hello, we are using Java.");
    }

    String message = "Java";
    TestingExceptions exception = new TestingExceptions(message);

    @Test
    public void testExceptionHandling() {
        message = "Programming using " + message;
        assertEquals(exception.printHiMessage(), message);
    }

    @Test(expected = ArithmeticException.class)
    public void testPrintMessage() throws ArithmeticException{
        exception.printMessage();
    }

    @Test(expected = java.lang.ArithmeticException.class)
    public void testPrintResult() throws ArithmeticException {
        exception.printResult();
    }

    @Test(expected = IOException.class)
    public void testReadingAndWritingFiles() throws IOException {
        ByteStreamsExample.readAndWrite("f.txt");
    }


}