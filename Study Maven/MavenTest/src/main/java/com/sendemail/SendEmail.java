package com.sendemail;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class SendEmail {
    public static void main(String[] args) {
        String to = "timeea.pitan@gmail.com";
        String from = "timeea.pitan@gmail.com";

        String host = "smtp.gmail.com";

        Properties properties = System.getProperties();

        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", "465");
        properties.put("mail.smtp.ssl.enable", "true");
        properties.put("mail.smtp.smtp.auth", "true");

        Session session = Session.getInstance(properties, new javax.mail.Authenticator(){
            protected PasswordAuthentication getPasswordAuthentication(){
                return new PasswordAuthentication("timeea.pitan@gmail.com", "*******");
            }
        });

        session.setDebug(true);

        try{
            MimeMessage message = new MimeMessage(session);

            message.setFrom(new InternetAddress(from));

            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

            message.setSubject("This is the subject line");

            // message.setText("This is actual message");

            message.setContent("<h1>This is actual message embedded in HTML tags</h1>", "text/html");

            System.out.println("sending...");

            Transport.send(message);

            System.out.println("Sent message successfully!");

        } catch (MessagingException mex){
            mex.printStackTrace();
        }
    }
}
