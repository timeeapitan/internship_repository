package com.wicket_part2.models.property_model;

import com.wicket_part2.models.compound_property_model.Customer;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;

public class MyForm extends Form<Customer> {

    private TextField<String> name;
    private TextField<String> street;

    public MyForm(String id) {
        super(id);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        Customer customer = Customer.builder().build();
        setModel(new Model<>(customer));
        name = new TextField<>("name", new PropertyModel<>(customer, "name"));
        street = new TextField<>("address.street", new PropertyModel<>(customer, "address.street"));
        add(name);
        add(street);
    }

    @Override
    protected void onSubmit() {
        Customer customer = getModel().getObject();
        customer.setName(name.getModelObject());
        customer.getAddress().setStreet(street.getModelObject());
        System.out.println("Name: " + customer.getName() + "\nStreet: " + customer.getAddress().getStreet());
    }
}
