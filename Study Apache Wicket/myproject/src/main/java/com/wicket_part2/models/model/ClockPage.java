package com.wicket_part2.models.model;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.IModel;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ClockPage extends WebPage {
    @Override
    protected void onInitialize() {
        super.onInitialize();
        IModel<String> clock = () -> {
            SimpleDateFormat df = new SimpleDateFormat("hh:mm:ss");
            return df.format(new Date());
        };

        add(new Label("clock", clock));

        add(new Link<>("refresh") {
            @Override
            public void onClick() {
            }
        });
    }
}
