package com.wicket_part2.models.bind_method;

import org.apache.wicket.markup.html.WebPage;

public class MyPage extends WebPage {
    @Override
    protected void onInitialize() {
        super.onInitialize();
        MyForm myForm;
        add(myForm = new MyForm("form"));
    }
}
