package com.wicket_part2.models.serialization;

import com.wicket_part2.models.compound_property_model.Address;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Builder
@ToString
@Data

public class Customer implements Serializable {
    private String name;
    private Address address;
}
