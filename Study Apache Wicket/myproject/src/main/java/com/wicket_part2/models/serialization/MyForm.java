package com.wicket_part2.models.serialization;

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.Model;

public class MyForm extends Form<Customer> {

    private TextField<String> name;
    private TextField<String> street;

    public MyForm(String id) {
        super(id);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        name = new TextField<>("name", new Model<>(""));
        street = new TextField<>("street", new Model<>(""));
        add(name);
        add(street);
    }

    @Override
    protected void onSubmit() {
        Customer customer = Customer.builder().build();
        customer.setName(name.getModelObject());
        customer.getAddress().setStreet(street.getModelObject());
        System.out.println("Name: " + customer.getName() + "\nStreet: " + customer.getAddress().getStreet());
    }
}
