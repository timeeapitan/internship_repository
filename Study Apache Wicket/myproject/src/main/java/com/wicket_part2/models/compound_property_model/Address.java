package com.wicket_part2.models.compound_property_model;

import lombok.*;

@Builder
@ToString
@Data
@Setter
@Getter
public class Address {
    private String street;
}
