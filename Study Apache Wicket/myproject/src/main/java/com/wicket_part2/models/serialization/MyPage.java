package com.wicket_part2.models.serialization;

import org.apache.wicket.markup.html.WebPage;

public class MyPage extends WebPage {
    @Override
    protected void onInitialize() {
        super.onInitialize();
        add(new MyForm("form"));
    }
}
