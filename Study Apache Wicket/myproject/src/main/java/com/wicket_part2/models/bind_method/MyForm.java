package com.wicket_part2.models.bind_method;

import com.wicket_part2.models.serialization.Customer;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.CompoundPropertyModel;

public class MyForm extends Form<Customer> {
    public MyForm(String id) {
        super(id);
        CompoundPropertyModel model = new CompoundPropertyModel(Customer.builder().build());
        setModel(model);

        add(new TextField<String>("name"));
        add(new TextField<String>("street", model.bind("address.street")));
    }

    @Override
    protected void onSubmit() {
        Customer customer = getModelObject();
        customer.setName(customer.getName());
        customer.setAddress(customer.getAddress());

        System.out.println("Name: " + customer.getName() + "\nStreet: " + customer.getAddress().getStreet());
    }
}
