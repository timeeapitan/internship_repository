package com.wicket_part2.models.compound_property_model;

import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.CompoundPropertyModel;

public class NestedExamplePage extends WebPage {
    private CompoundPropertyModel<Customer> model;

    public NestedExamplePage() {
        Customer customer = Customer.builder().build();
        customer.setAddress(Address.builder().build());
        customer.getAddress().setStreet("Penny Lane");

        setModel(new CompoundPropertyModel<>(customer));
        WebMarkupContainer parent = new WebMarkupContainer("address");
        parent.setOutputMarkupId(true);
        add(parent);
        parent.add(new Label("address.street"));
    }

    public CompoundPropertyModel<Customer> getModel() {
        return model;
    }

    public void setModel(CompoundPropertyModel<Customer> model) {
        this.model = model;
    }
}
