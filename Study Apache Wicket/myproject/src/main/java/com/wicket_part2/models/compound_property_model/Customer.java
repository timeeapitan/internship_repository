package com.wicket_part2.models.compound_property_model;

import lombok.*;
import java.io.Serializable;

@Builder
@ToString
@Data
@Getter
@Setter
public class Customer implements Serializable {
    private String name;
    private Address address;
}
