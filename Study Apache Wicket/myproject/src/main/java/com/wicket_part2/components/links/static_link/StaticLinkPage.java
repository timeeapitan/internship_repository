package com.wicket_part2.components.links.static_link;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;

public class StaticLinkPage extends WebPage {
    public StaticLinkPage(){}

    @Override
    protected void onInitialize() {
        super.onInitialize();
        add(new Label("message", "Hello, World!"));
    }
}
