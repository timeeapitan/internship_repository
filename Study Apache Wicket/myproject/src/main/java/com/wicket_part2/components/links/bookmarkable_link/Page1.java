package com.wicket_part2.components.links.bookmarkable_link;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;

public class Page1 extends WebPage {
    @Override
    protected void onInitialize() {
        super.onInitialize();
        add(new Label("page1", "This is page 1."));
    }
}
