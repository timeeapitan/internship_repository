package com.wicket_part2.components.links.bookmarkable_link;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;

public class MainPage extends WebPage {
    @Override
    protected void onInitialize() {
        super.onInitialize();
        add(new BookmarkablePageLink<>("link", ConfirmationRegistrationPage.class));
    }
}
