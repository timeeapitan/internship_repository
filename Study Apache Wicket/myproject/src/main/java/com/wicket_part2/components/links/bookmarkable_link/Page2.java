package com.wicket_part2.components.links.bookmarkable_link;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;

public class Page2 extends WebPage {
    @Override
    protected void onInitialize() {
        super.onInitialize();
        add(new Label("page2", "This is page 2."));
    }
}
