package com.wicket_part2.components.links.bookmarkable_link;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.request.mapper.parameter.PageParameters;

public class ConfirmationRegistrationPage extends WebPage {
    public ConfirmationRegistrationPage(PageParameters parameters) {
        super(parameters);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        add(new Label("label", "Welcome to the confirmation page. Please proceed with the following steps!"));
    }
}
