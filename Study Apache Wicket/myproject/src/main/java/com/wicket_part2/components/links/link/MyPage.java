package com.wicket_part2.components.links.link;

import com.wicket_part2.components.links.bookmarkable_link.Page1;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.link.Link;

public class MyPage extends WebPage {

    @Override
    protected void onInitialize() {
        super.onInitialize();
        add(new Link<Void>("myLink") {
            @Override
            public void onClick() {
                Page1 next = new Page1();
                setResponsePage(next);
            }
        });
    }
}
