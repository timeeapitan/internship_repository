package com.example.helloworldexample;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.request.mapper.parameter.PageParameters;

public class AnotherPage extends WebPage {
    public AnotherPage(PageParameters parameters) {
        super(parameters);
    }
}
