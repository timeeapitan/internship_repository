package com.example.markupinheritance;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.request.mapper.parameter.PageParameters;

public class TemplatePage extends WebPage {
    public static final String CONTENT_ID = "contentComponent";

    private HeaderPanel headerPanel;
    private MenuPanel menuPanel;
    private FooterPanel footerPanel;

    public TemplatePage(PageParameters parameters) {
        super(parameters);

        //add(new HeaderPanel("headerPanel"));
        add(new MenuPanel("menuPanel"));
        //add(footerPanel = new FooterPanel("footerPanel"));
       // add(new Label(CONTENT_ID, "Put your content here"));
    }

}
