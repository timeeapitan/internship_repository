package com.example.navomatic;

import org.apache.wicket.markup.html.border.Border;
import org.apache.wicket.*;

public class NavomaticBorder extends Border {

    public NavomaticBorder(String id) {
        super(id);

        addToBorder(new MyBorder("navigationBorder"));
        addToBorder(new MyBorder("bodyBorder"));
    }
}
