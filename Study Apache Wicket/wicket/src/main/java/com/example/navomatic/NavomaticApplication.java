package com.example.navomatic;

import org.apache.wicket.Page;
import org.apache.wicket.protocol.http.WebApplication;

public class NavomaticApplication extends WebApplication {
    @Override
    public Class<? extends Page> getHomePage() {
        return Page1.class;
    }
}
