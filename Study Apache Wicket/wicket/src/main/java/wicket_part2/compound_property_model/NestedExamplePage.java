package wicket_part2.compound_property_model;

import org.apache.wicket.Page;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.CompoundPropertyModel;

public class NestedExamplePage extends Page {
    CompoundPropertyModel model;

    public NestedExamplePage() {
        Customer customer = new Customer();
        customer.setAddress(new Address());
        customer.getAddress().setStreet("Penny Lane");

        setModel(new CompoundPropertyModel(customer));
        WebMarkupContainer parent = new WebMarkupContainer("address");
        add(parent);
        parent.add(new Label("address.street"));
    }

    public CompoundPropertyModel getModel() {
        return model;
    }

    public void setModel(CompoundPropertyModel model) {
        this.model = model;
    }
}
