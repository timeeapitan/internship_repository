package wicket_part2.compound_property_model;

public class Customer {
    Address address;

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
