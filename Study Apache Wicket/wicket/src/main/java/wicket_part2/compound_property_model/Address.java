package wicket_part2.compound_property_model;

public class Address {
    String street;

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }
}
