package wicket_part2.model;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.Model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ClockPage extends WebPage {
    public ClockPage() {
        Model clock = new Model() {
            @Override
            public Serializable getObject() {
                SimpleDateFormat df = new SimpleDateFormat("hh:mm:ss");
                String time = df.format(new Date());
                return time;
            }
        };

        add(new Label("clock", clock));
        add(new Link<Void>("refresh") {
            @Override
            public void onClick() {} // it is empty so the page will be refreshed
        });
    }
}
